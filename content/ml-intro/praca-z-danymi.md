---
title: "Wstęp do uczenia maszynowego - part II"
subtitle: Praca z danymi
date: 2020-08-01
location: "Gdynia, Polska"
tags: ["courses", "WWW"]
---


### 2.0. Wczytywanie danych

Pierwszym krokiem w każdym przedsięwzięciu opartym na uczeniu maszynowym jest pobranie i umieszczenie w systemie czystych, niezmodyfikowanych danych. Przykładami takich danych są plik dziennika zdarzeń, plik zbioru danych i baza danych. Co więcej, często zachodzi potrzeba pobrania danych pochodzących z wielu źródeł. Receptury omówione w tym rozdziale przedstawiają
metody wczytywania danych z różnych źródeł, takich jak plik w formacie CSV i baza danych SQL.

Pokazano metody generowania symulowanych danych wraz z właściwościami niezbędnymi do przeprowadzania eksperymentów. Wprawdzie w ekosystemie Pythona jest wiele różnych sposobów na wczytywanie danych, ale pokazany zostanie oferowany przez bibliotekę pandas rozbudowany zestaw metod przeznaczonych do wczytywania danych zewnętrznych. Natomiast do generowania danych symulowanych można użyć scikit-learn, dostępnej jako open source biblioteki uczenia maszynowego.

### 2.1. Wczytywanie przykładowego zbioru danych
Jak wczytać już istniejący zbiór danych?
Biblioteka scikit-learn jest dostarczana wraz z pewną liczbą popularnych zbiorów danych przeznaczonych do użycia.
```
# Wczytanie zbiorów danych biblioteki scikit-learn.
from sklearn import datasets
# Wczytanie zbioru danych w postaci cyfr.
digits = datasets.load_digits()
# Utworzenie macierzy cech.
features = digits.data

# Utworzenie wektora docelowego.
target = digits.target
# Wyświetlenie pierwszej obserwacji.
features[0]
```

Analiza
Bardzo często nie chcesz zajmować się zadaniami związanymi z wczytaniem, przekształceniem i oczyszczeniem rzeczywistego zbioru danych, zanim nie wypróbujesz pewnego algorytmu lub metody uczenia maszynowego. Na szczęście biblioteka scikit-learn jest dostarczana wraz z przykładowymi zbiorami danych, które można bardzo łatwo wykorzystać. 

Te zbiory danych są często określane mianem „zabawek” (ang. toy), ponieważ są znacznie mniejsze i bardziej przejrzyste niż te, które będą używane w rzeczywistości. Oto lista najpopularniejszych przykładowych zbiorów danych dostarczanych wraz z biblioteką scikit-learn:

**load_boston**
Zawiera 503 obserwacje cen domów w Bostonie. To dobry zbiór danych do sprawdzania algorytmów regresji.

**load_iris**
Zawiera 150 obserwacji dotyczących rozmiaru kwiatu irysa. To dobry zbiór danych do sprawdza-
nia algorytmów klasyfikacji.

**load_digits**
Zawiera 1797 obserwacji obrazów wraz z ręcznie zapisanymi cyframi. To dobry zbiór danych
do sprawdzania algorytmów przeznaczonych do uczenia klasyfikacji obrazu.  


Linki:
  * [Podrozdział Toy datasets w dokumentacji scikit-learn](http://scikit-learn.org/stable/datasets/index.html#toy-datasets)
  * [Artykuł The Digit Dataset w dokumentacji scikit-learn](http://scikit-learn.org/stable/auto_examples/datasets/plot_digits_last_image.html)


## CDN

### 3.0. Przygotowywanie danych

Przygotowywanie danych (ang. data wrangling) to dość często używane pojęcie, najczęściej w celu
opisania procesu przekształcenia niezmodyfikowanych danych na postać czystego i  zorganizowanego formatu informacji gotowych do użycia. To tylko jeden — choć zarazem niezwykle ważny — krok na etapie wstępnego przetwarzania danych.
Najczęściej wykorzystywaną strukturą stosowaną do przygotowywania danych jest tzw. ramka
danych, która jest intuicyjna w użyciu i jednocześnie niezwykle elastyczna. Ramka danych ma postać tabelaryczną, co oznacza, że została oparta na wierszach i kolumnach, podobnie jak dane przechowywane w arkuszu kalkulacyjnym. Oto przykład ramki danych utworzonej na podstawie informacji o pasażerach Titanica:

```
# Wczytanie biblioteki.
import pandas as pd

# Utworzenie adresu URL.
url = 'https://tinyurl.com/titanic-csv'

# Umieszczenie wczytanych informacji w ramce danych.
dataframe = pd.read_csv(url)

# Wyświetlenie pierwszych pięciu wierszy.
dataframe.head(5)
```
## CDN