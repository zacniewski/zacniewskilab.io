---
title: "Wstęp do uczenia maszynowego - part IV"
subtitle: Praca z obrazami
date: 2020-08-01
location: "Gdynia, Polska"
tags: ["courses", "WWW"]
---


### 8.0. Wprowadzenie

Klasyfikacja obrazu to jeden z najbardziej ekscytujących obszarów uczenia maszynowego. Możliwość
rozpoznawania przez komputer wzorców i obiektów w obrazach jest naprawdę nieoceniona. Jednak zanim będzie można wykorzystać uczenie maszynowe z obrazami, najpierw trzeba przekształcić te obrazy na cechy możliwe do użycia z algorytmami uczenia maszynowego.

Do pracy z obrazami doskonale nadaje się biblioteka OpenCV (ang. open source computer vision). Choć istnieje
wiele dobrych bibliotek przeznaczonych do pracy z obrazami, to jednak OpenCV zalicza się do najpopularniejszych, a ponadto jest doskonale udokumentowana. 

## CDN