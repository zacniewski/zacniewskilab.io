---
title: "Wstęp do uczenia maszynowego - part III"
subtitle: Praca z tekstem
date: 2020-08-01
location: "Gdynia, Polska"
tags: ["courses", "WWW"]
---

### 6.0. Obsługa tekstu

Pozbawiony struktury tekst, taki jak zawartość książki lub wiadomość w serwisie Twitter, to jedno
z najbardziej interesujących źródeł cech i jednocześnie najtrudniejsze do obsłużenia. W rozdziale
omówię strategie przekształcania tekstu na cechy dostarczające wielu informacji. Nie oznacza to jed-
nak, że zaprezentowane tutaj receptury są wyczerpujące. Istnieją całe dyscypliny akademickie
koncentrujące się na obsłudze tekstu i podobnych typów danych, a opis stosowanych w tym zakresie
technik mógłby wypełnić małą bibliotekę. Mimo to można mówić tu o kilku najczęściej używanych
technikach, które będą nieocenionymi narzędziami w arsenale każdego, kto zajmuje się przetwarza-
niem tekstu.

## CDN
