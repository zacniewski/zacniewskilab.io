## Welcome!

This site contains materials for students from my [department](https://informatyka.amw.gdynia.pl/). There are also some information [about](pages/about/) me.

## The site will be not updated anymore!
### Now, the living page will be available [here](https://zacniewski.github.io)!

