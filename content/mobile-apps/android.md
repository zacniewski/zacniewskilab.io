---
title: "Aplikacje mobilne - Android"
date: 2020-08-01
location: "Gdynia, Polska"
tags: ["courses", "WWW"]
---

### Linki
  * [oficjalna dokumentacja Androida](https://developer.android.com/)
  * [niezbędnik programisty Androida](https://guides.codepath.com/android/must-have-libraries)

