---
title: "Aplikacje mobilne - React Native"
date: 2020-10-01
location: "Gdynia, Polska"
tags: ["courses", "mobile"]
---

### Dokumentacja
  * [oficjalna dokumentacja React Native](https://reactnative.dev/docs/getting-started) (może nie działać na Firefoxie!),
  * [Expo - zestaw narzędzi do współpracy z React Native](https://expo.io/),
  * [wstęp do Expo](https://docs.expo.io/).


### Ciekawostki
  * [dlaczego warto używać React-Native?](https://www.stxnext.com/blog/why-use-react-native-your-mobile-app/),
  * [współpraca React Native i Expo](https://blog.swmansion.com/wprowadzenie-do-expo-i-react-native-fe1551aba0a6),
  * [różnice między ReactJS a React-Native](https://medium.com/@alexmngn/from-reactjs-to-react-native-what-are-the-main-differences-between-both-d6e8e88ebf24),
  * [dyskusja na Stack Overflow na temat jak wyżej](https://stackoverflow.com/questions/34641582/what-is-the-difference-between-react-native-and-react),
  * [aplikacje mobilne z różnych perspektyw](https://itcraftapps.com/pl/blog/).

