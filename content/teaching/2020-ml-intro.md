---
title: "Wstęp do Data Science"
subtitle: semestr zimowy 2020/2021
date: 2020-10-06
tags: ["courses", "data-science"]

---

Tematy zadań  
======  

### 1. Powtórka operacji na tablicach i macierzach (wskazane Numpy).
  * należy utworzyć repozytorium np. o nazwie `wstep-do-ml-dane-175ic` lub innej identyfikującej przedmiot i grupę jednoznacznie, gdzie zamiast 'dane' wpisujemy nazwisko lub nr indeksu; będzie ono podstawą zaliczenia przedmiotu,
  * utwórz tablicę zawierającą 10 zer,  
  * utwórz tablicę zawierającą 10 piątek,  
  * utwórz tablicę zawierającą liczby od 10 do 50,  
  * utwórz macierz (tablica wielowymiarowa) o wymiarach 3x3 zawierającą liczby od 0 do 8,  
  * utwórz macierz jednostkową o wymiarach 3x3,
  * utwórz macierz o wymiarach 5x5 zawierającą liczby z dystrybucji normalnej (Gaussa),
  * utwórz macierz o wymiarach 10x10 zawierającą liczby od 0,01 do 1 z krokiem 0,01,
  * utwórz tablicę zawierającą 20 liniowo rozłożonych liczb między 0 a 1 (włącznie z 0 i 1),
  * utwórz tablicę zawierającą losowe liczby z przedziału (1, 25), następnie zamień ją na macierz o wymiarach 5 x 5 z tymi samymi liczbami:
      * oblicz sumę wszystkich liczb w ww. macierzy,
      * oblicz średnią wszystkich liczb w ww. macierzy,
      * oblicz standardową dewiację dla liczb w ww. macierzy,
      * oblicz sumę każdej kolumny ww. macierzy i zapisz ją do tablicy.
  * utwórz macierz o wymiarach 5x5 zawierającą losowe liczby z przedziału (0, 100) i:
      * oblicz medianę tych liczb,
      * znajdź najmniejszą liczbę tej macierzy,
      * znajdź największą liczbę tej macierzy.
  * utwórz macierz o wymiarach różnych od siebie i większych od 1, zawierającą losowe liczby z przedziału (0, 100) i dokonaj jej transpozycji,
  * utwórz dwie macierze o odpowiednich wymiarach (doczytać), większych od 2 i dodaj je do siebie,
  * utwórz dwie macierze o odpowiednich wymiarach (doczytać) różnych od siebie i większych od 2, a następnie pomnóż je przez siebie za pomocą dwóch różnych funkcji (np. 'matmul' i 'multiply'),
  * świetne [wprowadzenie](https://realpython.com/numpy-tutorial/) do NumPy z RealPython,
  * ściąga do pakietu Numpy - [tutaj](https://s3.amazonaws.com/assets.datacamp.com/blog_assets/Numpy_Python_Cheat_Sheet.pdf)

### 2. Analiza danych z pakietem Pandas
  * instalacja: ```pip install pandas --user```
  * oficjalna ściąga - [tutaj](https://pandas.pydata.org/Pandas_Cheat_Sheet.pdf),
  * krótki wstęp do Pandas -[tutaj](https://pandas.pydata.org/pandas-docs/stable/getting_started/10min.html),
  * podstawowe struktury danych w Pandas - [tutaj](https://pandas.pydata.org/pandas-docs/stable/getting_started/dsintro.html#dsintro) (szybkie zapoznanie się ze strukturami Series i DataFrame zawartymi w tym linku jest wkazane),
  * roboczy plik tekstowy 'samochody1tys.csv', zawierający dane z 1000 ogłoszeń motoryzacyjnych, dostępny jest na Slacku. Dane składają się z 1000 rekordów. Każdy rekord dotyczy jednego ogłoszenia motoryzacyjnego i zawiera następujące informacje: identyfikator ogłoszenia, marka samochodu, model samochodu, rok produkcji, rodzaj silnika, pojemność silnika, przebieg samochodu, cena, województwo.
  * do wczytania danych korzystamy z funkcji ```read_csv```, 
  * GUI do biblioteki pandas - [tutaj](https://github.com/adamerose/pandasgui)
  * przed każdą operacją (zbiorem operacji) konieczna jest linia z komentarzem po polsku, a w niej opis co zamierzamy zrobić,
  * na danych z ww. należy użyć  10 wybranych metod z pakietu pandas (nie licząc ```read_csv```),
  * należy wykonać zrzut ekranu dla każdej z ww. metod i dołączyć te obrazki do repo.


### 3. Praca ze SciPy i NumPy
  * materiał źródłowy - [tutaj](https://www.utc.fr/~jlaforet/Suppl/python-cheatsheets.pdf),
  * należy zainstalować wymagane biblioteki, najlepiej w środowisku wirtualnym,
  * z każdej ściągi (oprócz 'Jupyter Notebook') należy wybrać 5 przykładów funkcji, z kolumny drugiej i/lub trzeciej (pierwsza jest za łatwa) i je uruchomić oraz krótko opisać.


### 4. Web scraping i zapis do Excela
  * instalacja Beautiful Soup: ```pip install beautifulsoup4 --user``` (do web scrapingu),
  * dokumentacja BS4 - [tutaj](https://beautiful-soup-4.readthedocs.io/en/latest/),
  * instalacja openpyxl: ```pip install openpyxl --user``` (do pracy z arkuszami Excela),
  * dokumentacja openpyxl - [tutaj](https://openpyxl.readthedocs.io/en/stable/index.html),
  * wstęp do openpyxl - [tutaj](https://openpyxl.readthedocs.io/en/stable/tutorial.html),
  * wstęp do web scrapingu za pomocą BS4 - [tutaj](https://www.kodolamacz.pl/blog/wyzwanie-python-7-web-scraping/),
  * nie korzystamy z dystrybucji Anaconda! Można zainstalować pakiet requests za pomocą pip'a,
  * utwórz za pomocą openpyxl trzy arkusze o nazwach "Giełda", "Linki" i "Filmweb" (pokazane we wstępie),
  * pod [adresem](https://stooq.pl/q/?s=cdr) znajdziesz notowania giełdowe spółki CD Projekt Red, 
      * wygeneruj 5 losowych kodów 3-literowych,
      * generowane są trzyliterowe kody do momentu aż trafi się 5, które istnieją. 
      * inna opcja - na  stronie z błędem 404 (dla nieistniejącego kodu) jest link do najbardziej pasującego rekordu, można go wyciągnąć za pomocą BS4 i stworzyć kolejne zapytanie,
      * napisz kod, który dla wygenerowanych wyżej trzyliterowych kodów spółki wyświetli ich aktualną cenę (Kurs), procentową zmianę (Zmiana) oraz liczbę transakcji (Transakcje),
      * zapisz wyniki do arkusza 'Giełda',
  * dla wybranej strony internetowej napisz kod, który połączy się ze stroną, znajdzie wszystkie linki (co najmniej 10) na stronie (znacznik 'a' posiadający atrybut 'href'), a następnie zapisze je do arkusza 'Linki', 
  * dla ustalonego linku do filmu na Filmwebie, np. [tego](ttps://www.filmweb.pl/film/To%3A+Rozdzia%C5%82+2-2019-793838), napisz kod, który zwróci:
      * reżysera,
      * datę premiery,
      * boxoffice,
      * ocenę filmu.    
      * zapisz uzyskane wyniki do arkusza 'Filmweb'
  * zapisz plik z wynikami trzech zadań w formie 'nazwisko-grupa.xlsx'.

### 5. Praca z tekstem - spaCy
  * tutorial źródłowy - [tutaj](https://realpython.com/natural-language-processing-spacy-python/),
  * model spaCy dla języka polskiego dostępny [tutaj](https://spacy.io/models)
  * należy pobrać ww. model wg instrukcji na stronie, pakiety instalować najlepiej w środowisku wirtualnym,
  * należy wykonać ćwiczenia z ww. tutoriala, ale dla modelu języka polskiego oraz krótko je opisać.

### 6. Praca z plikami
  * tutorial źródłowy - [tutaj](https://realpython.com/read-write-files-python/),
  * pliki w ćwiczeniu powinny mieć w nazwie nazwisko lub numer albumu wykonawcy,
  * należy krótko opisać w pliku README.md wykorzystane funkcje oraz załączyć używane w zadaniu ww. pliki.

### 7. Prosta sieć neuronowa do implementacji funkcji logicznych
  * można wykonać to zadanie w dowolnym języku,
  * krótki wstęp do sieci neuronowych wraz z przykładami dotyczącymi funkcji logicznych - [link nr 1 tutaj](https://www.analyticsvidhya.com/blog/2016/03/introduction-deep-learning-fundamentals-neural-networks/),
  * należy zaimplementować bramki logiczne AND, OR, NOT za pomocą pojedynczego neurona (przykłady 1-3),
  * funkcja aktywacji <i>f</i> pokazana przed pierwszym przykładem jest wystarczająca,
  * należy zaimplementować bramki logiczne XOR i XNOR za pomocą sieci neuronów (dalsze przykłady),
  * można wykorzystać wartości wag i bias'ów podane na ww. stronie,
  * proszę pamiętać, że każdy neuron ma funkcję aktywacji,
  * w tabeli do każdego przykładu powinny znaleźć się:
      * wartości wejść,
      * wartości wag i biasów,
      * wartości sumy iloczynów wag i wejść,
      * wartości funkcji aktywacji neuronów (czyli ich wyjście),
  * model neuronu pokazano [tutaj](http://galaxy.uci.agh.edu.pl/~vlsi/AI/wstep/).

### 8. Praca z danymi w formacie JSON i CSV
  * tutorial źródłowy do formatu JSON- [tutaj](https://realpython.com/python-json/),
  * tutorial źródłowy do formatu CSV- [tutaj](https://realpython.com/python-csv/),
  * zasoby do pracy z formatem JSON - [tutaj](https://jsonplaceholder.typicode.com/) (dział 'Resources'),
  * należy wykonać ćwiczenia z ww. tutoriali, dla własnych danych,
  * tam gdzie są linki do ww. źródła danych JSON należy użyć innych zasobów (linków) z tej strony,
  * należy krótko opisać w pliku README.md wykorzystane klasy i funkcje.


### 9. Praca z obrazami - OpenCV + Python
  * należy zainstalować pakiet OpenCV,
  * najwygodniejszy sposób: [tutaj](https://www.pyimagesearch.com/2018/09/19/pip-install-opencv/),
  * wskazane sposoby instalacji (dłuższe, ale bardziej wszechstronne, z dostępem np. do C++ API): [tutaj](https://www.learnopencv.com/opencv-installation-on-ubuntu-macos-windows-and-raspberry-pi/),
  * proszę przerobić [tutorial z PyImage Search](https://www.pyimagesearch.com/2018/07/19/opencv-tutorial-a-guide-to-learn-opencv/),
  * należy użyć własnych obrazków!,
  * przetworzone obrazy wraz z opisem użytych funkcji należy wrzucić do pliku README.md, ale utworzonego wewnątrz folderu z daną laborką (nie do głównego).
  

### 10. Współbieżność w Pythonie
  * strona źródłowa nr 1 - [tutaj](https://realpython.com/python-concurrency/),
  * strona źródłowa nr 2 - [tutaj](https://stackabuse.com/concurrency-in-python/),
  * moduły analizowane w tym zadaniu to: ```threading```, ```multiprocessing``` i ```asyncio```,
  * można użyć API od [Picsum](https://picsum.photos/v2/list) do pobierania obrazków lub innego, pozwalającego na pobieranie plików przez czas pozwalający zaobserwować różnice w szybkości ww. modułów,
  * należy przerobić przykłady z ww. stron i załączyć porównanie wyników czasowych.

Zasady zaliczenia przedmiotu
======

1. Dowolny język do realizacji zadania. Aczkolwiek zalecany Python i jego biblioteki, takie jak Numpy, Scipy, Matplotlib, pandas itp.
  * idealny byłby Jupyter Notebook - <a href="https://www.dataquest.io/blog/jupyter-notebook-tutorial/">szybkie intro</a>
  * aczkolwiek zrzuty z konsoli np. IPython lub standardowej też są w porządku.
2. Zadanie należy zrealizować do następnych zajęć - max. 5 punktów,
  * opóźnienie skutkuje obniżoną liczbą punktów za zadanie (-2 do tygodnia i -4 powyżej tygodnia)
3. Zrealizowane zadanie należy umieścić na portalu GitHub (lub BitBucket, GitLab) i wysłać link do repozytorium do prowadzącego (Slack).
4. Należy wykonać ww. zrzuty ekranu z wynikami i również umieścić je w repozytorium.
5. Zadanie należy zaprezentować osobiście prowadzącemu (najlepiej na bieżących lub kolejnych zajęciach). 
