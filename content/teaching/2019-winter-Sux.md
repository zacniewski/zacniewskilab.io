---
title: "Wstęp do Data Science"
subtitle: semestr zimowy 2019/2020
#type: "Wykłady i laboratoria"
#permalink: /teaching/2019-data-science-intro
#venue: "AMW, WEM"
date: 2019-10-06
#location: "Gdynia, Polska"
tags: ["courses", "data-science"]

---

Tematy zadań  
======  

### 1. Powtórka operacji na tablicach i macierzach (wskazane Numpy).

  * utwórz tablicę zawierającą 10 zer,  
  * utwórz tablicę zawierającą 10 piątek,  
  * utwórz tablicę zawierającą liczby od 10 do 50,  
  * utwórz macierz (tablica wielowymiarowa) o wymiarach 3x3 zawierającą liczby od 0 do 8,  
  * utwórz macierz jednostkową o wymiarach 3x3,
  * utwórz macierz o wymiarach 5x5 zawierającą liczby z dystrybucji normalnej (Gaussa),
  * utwórz macierz o wymiarach 10x10 zawierającą liczby od 0,01 do 1 z krokiem 0,01,
  * utwórz tablicę zawierającą 20 liniowo rozłożonych liczb między 0 a 1 (włącznie z 0 i 1),
  * utwórz tablicę zawierającą losowe liczby z przedziału (1, 25), następnie zamień ją na macierz o wymiarach 5 x 5 z tymi samymi liczbami:
      * oblicz sumę wszystkich liczb w ww. macierzy,
      * oblicz średnią wszystkich liczb w ww. macierzy,
      * oblicz standardową dewiację dla liczb w ww. macierzy,
      * oblicz sumę każdej kolumny ww. macierzy i zapisz ją do tablicy.
  * utwórz macierz o wymiarach 5x5 zawierającą losowe liczby z przedziału (0, 100) i:
      * oblicz medianę tych liczb,
      * znajdź najmniejszą liczbę tej macierzy,
      * znajdź największą liczbę tej macierzy.
  * utwórz macierz o wymiarach różnych od siebie i większych od 1, zawierającą losowe liczby z przedziału (0, 100) i dokonaj jej transpozycji,
  * utwórz dwie macierze o odpowiednich wymiarach (doczytać), większych od 2 i dodaj je do siebie,
  * utwórz dwie macierze o odpowiednich wymiarach (doczytać) różnych od siebie i większych od 2, a następnie pomnóż je przez siebie za pomocą dwóch różnych funkcji (np. 'matmul' i 'multiply'),
  * ściąga do pakietu Numpy - [tutaj](https://s3.amazonaws.com/assets.datacamp.com/blog_assets/Numpy_Python_Cheat_Sheet.pdf)
  * dla spóźnialskich: należy krótko opisać i uruchomić wszystkie funkcje (od 'add' do 'lcm') z sekcji 'Math operations' na [stronie](https://docs.scipy.org/doc/numpy/reference/ufuncs.html).

### 2. Analiza danych z pakietem Pandas
  * instalacja: ```pip install pandas --user```
  * oficjalna ściąga - [tutaj](https://pandas.pydata.org/Pandas_Cheat_Sheet.pdf),
  * krótki wstęp do Pandas -[tutaj](https://pandas.pydata.org/pandas-docs/stable/getting_started/10min.html),
  * podstawowe struktury danych w Pandas - [tutaj](https://pandas.pydata.org/pandas-docs/stable/getting_started/dsintro.html#dsintro) (szybkie zapoznanie się ze strukturami Series i DataFrame zawartymi w tym linku jest wkazane),
  * roboczy plik tekstowy 'samochody1tys.csv', zawierający dane z 1000 ogłoszeń motoryzacyjnych, dostępny jest na Slacku. Dane składają się z 1000 rekordów. Każdy rekord dotyczy jednego ogłoszenia motoryzacyjnego i zawiera następujące informacje: identyfikator ogłoszenia, marka samochodu, model samochodu, rok produkcji, rodzaj silnika, pojemność silnika, przebieg samochodu, cena, województwo.
  * należy przerobić krótki wstęp do Pandas (link powyżej) i tam gdzie się da należy użyć danych z pliku roboczego,
  * do wczytania danych korzystamy z funkcji ```read_csv```, 
  * przed każdą operacją (zbiorem operacji) konieczna jest linia z komentarzem po polsku, a w niej opis co zamierzamy zrobić,
  * wyniki zapisujemy w pliku .ipynb.

### 3. Matplotlib
  * instalacja: ```pip install matplotlib --user```
  * krótki wstęp po polsku - [tutaj](https://python101.readthedocs.io/pl/latest/pylab/),
  * ściąga - [tutaj](https://s3.amazonaws.com/assets.datacamp.com/blog_assets/Python_Matplotlib_Cheat_Sheet.pdf),
  * utwórz wykresy funkcji liniowej, kwadratowej, trzeciego rzędu oraz wybranej funkcji trygonometrycznej zgodnie z [wytycznymi](https://matplotlib.org/tutorials/introductory/usage.html#sphx-glr-tutorials-introductory-usage-py),
  * utwórz wykres słupkowy (bar chart) na podstawie danych z pliku 'samochody1tys.csv' (lab. nr 2). Może to być np. liczebność danej marki samochodu. Można użyć pakietów numpy i pandas, jeśli jest taka potrzeba,
  * utwórz wykres kołowy (pie chart) na podstawie ww. pliku, ale z innymi danymi niż do wykresu słupkowego,
  * utwórz histogram na podstawie ww. pliku, ale z innymi danymi niż poprzednie,
  * wszystkie wykresy powinny być opisane wg prawideł tworzenia wykresów (tytuły, opisy osi, jednostki, itp. itd.), 
  * proszę pamiętać o komentarzach przed użyciem danej funkcji,


### 4. Praca z danymi - web scraping i zapis do Excela
  * instalacja Beautiful Soup: ```pip install beautifulsoup4 --user``` (do web scrapingu),
  * dokumentacja BS4 - [tutaj](https://beautiful-soup-4.readthedocs.io/en/latest/),
  * instalacja openpyxl: ```pip install openpyxl --user``` (do pracy z arkuszami Excela),
  * dokumentacja openpyxl - [tutaj](https://openpyxl.readthedocs.io/en/stable/index.html),
  * wstęp do openpyxl - [tutaj](https://openpyxl.readthedocs.io/en/stable/tutorial.html),
  * wstęp do web scrapingu za pomocą BS4 - [tutaj](https://www.kodolamacz.pl/blog/wyzwanie-python-7-web-scraping/),
  * nie korzystamy z dystrybucji Anaconda! Można zainstalować pakiet requests za pomocą pip'a,
  * utwórz za pomocą openpyxl trzy arkusze o nazwach "Giełda", "Linki" i "Filmweb" (pokazane we wstępie),
  * pod [adresem](https://stooq.pl/q/?s=cdr) znajdziesz notowania giełdowe spółki CD Projekt Red, 
      * wygeneruj 5 losowych kodów 3-literowych,
      * generowane są trzyliterowe kody do momentu aż trafi się 5, które istnieją. 
      * inna opcja - na  stronie z błędem 404 (dla nieistniejącego kodu) jest link do najbardziej pasującego rekordu, można go wyciągnąć za pomocą BS4 i stworzyć kolejne zapytanie,
      * napisz kod, który dla wygenerowanych wyżej trzyliterowych kodów spółki wyświetli ich aktualną cenę (Kurs), procentową zmianę (Zmiana) oraz liczbę transakcji (Transakcje),
      * zapisz wyniki do arkusza 'Giełda',
  * dla swojej ulubionej strony internetowej napisz kod, który połączy się ze stroną, znajdzie wszystkie linki na stronie (znacznik 'a' posiadający atrybut 'href'), a następnie zapisze je do arkusza 'Linki', 
  * dla ustalonego linku do filmu na Filmwebie, np. [tego](ttps://www.filmweb.pl/film/To%3A+Rozdzia%C5%82+2-2019-793838), napisz kod, który zwróci:
      * reżysera,
      * datę premiery,
      * boxoffice,
      * ocenę filmu.    
      * zapisz uzyskane wyniki do arkusza 'Filmweb'
  * zapisz plik z wynikami trzech zadań w formie 'nazwisko-grupa.xlsx'.

### 5. Regresja liniowa z scikit-learn
  * instalacja: ```pip install scikit-learn --user```,
  * krótko o Machine Learning - [link nr 1 tutaj](https://www.statystyczny.pl/co-to-jest-machine-learning/),
  * kilka słów o regresji liniowej - [link nr 2 tutaj](https://www.statystyczny.pl/regresja-liniowa/),
  * wstęp do regresji liniowej z przykładami w scikit-learn [link nr 3 tutaj](https://ksopyla.com/machine-learning/modele-regresji-liniowej-z-scikit-learn/),
  * o zastosowaniu regresji liniowej - [link nr 4 tutaj](https://matematyka.poznan.pl/artykul/regresja-liniowa-czyli-o-zastosowaniu-funkcji-liniowej-w-analizie-statystycznej/),
  * krótko o prostej regresji - [link nr 5 tutaj](https://cyrkiel.info/statystyka/prosta-regresji/),
  * w scikit-learn [dostępne](https://scikit-learn.org/stable/datasets/index.html) są dwa proste zbiory treningowe dla regresji liniowej: 'diabetes' i 'boston',
  * korzystając z linka nr 3 należy:
      * opracować wizualizację dla zbioru 'boston',
      * porównać modele regresji liniowej dla zbioru 'diabetes',
  * opracować przykład z linka nr 2 z wykorzystaniem bibliotek Python'a,
  * opracować przykład z linka nr 4 z wykorzystaniem bibliotek Python'a,
  * wygenerować zbiór 100 punktów oraz obliczyć i wyświetlić dla nich prostą regresji (z wykorzystaniem bibliotek Python'a), dla wyższej oceny należy to zzadanie zrealizować wg kolejnych kroków z linka nr 5,
  * wszystkie wykresy powinny być opisane wg prawideł tworzenia wykresów (tytuły, opisy osi, jednostki, itp. itd.).

### 6. Klasyfikacja z użyciem z scikit-learn
  * w tej laborce tworzymy 3 odzielne notebooki - każdy dla jednego zadania,
  * zadanie nr 1: należy przerobić klasyczny przykład klasyfikacji (zbiór IRIS) - [link nr 1 tutaj](https://kssk.gitbook.io/msi/1.-klasyfikacja-jako-przyklad-rozpoznawania-wzorcow/1.1-struktura-zbioru-danych),
  * zadanie nr 2: należy przerobić pokazane przykłady klasyfikacji i wykonać wszystkie cztery wewnętrzne zadania - [link nr 2 tutaj](https://sebkaz.github.io/DataMining/05_Modelowanie_cw/"),
  * zadanie nr 3: należy przerobić przykład klasyfikacji polskich tekstów - [link nr 3 tutaj](http://www.deepdata.pl/nlp-scikit-learn-klasyfikacja/przyklad-klasyfikacji-polskich-tekstow-czesc-1/),
  * proszę pamiętać o importach odpowiednich pakietów!

### 7. Prosta sieć neuronowa do implementacji funkcji logicznych
  * można wykonać to zadanie w dowolnym języku,
  * krótki wstęp do sieci neuronowych wraz z przykładami dotyczącymi funkcji logicznych - [link nr 1 tutaj](https://www.analyticsvidhya.com/blog/2016/03/introduction-deep-learning-fundamentals-neural-networks/),
  * należy zaimplementować bramki logiczne AND, OR, NOT za pomocą pojedynczego neurona (przykłady 1-3),
  * funkcja aktywacji <i>f</i> pokazana przed pierwszym przykładem jest wystarczająca,
  * należy zaimplementować bramki logiczne XOR i XNOR za pomocą sieci neuronów (dalsze przykłady),
  * można wykorzystać wartości wag i bias'ów podane na ww. stronie,
  * proszę pamiętać, że każdy neuron ma funkcję aktywacji,
  * w tabeli do każdego przykładu powinny znaleźć się:
      * wartości wejść,
      * wartości wag i biasów,
      * wartości sumy iloczynów wag i wejść,
      * wartości funkcji aktywacji neuronów (czyli ich wyjście),
  * model neuronu pokazano [tutaj](http://galaxy.uci.agh.edu.pl/~vlsi/AI/wstep/).

### 8. Algorytm k najbliższych sąsiadów (kNN)
  * teoria do lab. nr 8-10 przesłana na Slacku,
  * przydatne pliki do lab. 8-10 - [tutaj](ftp://ftp.helion.pl/przyklady/dascpo.zip),
  * przenalizuj przykład z ulubionym językami programowania (dane z DataSciencester),
  * wykonaj implementację funkcji plot_state_borders(), ale dla kraju **innego** niż USA,
  * należy pamiętać o aktualizacji współrzędnych geograficznych,
  * dokonaj graficznej interpretacji algorytmu dla k od 1 do 9 (podobnie jak na rysunkach 12.2 - 12.4),
  * zrealizuj przykład z danymi o większej liczbie wymiarów dla innych danych niż w opisie,
  * na wyższą ocenę - za pomocą pakietu [scikit-learn](https://scikit-learn.org/stable/modules/neighbors.html)</a> przedstaw dwie dowolne implementacje algorytmu kNN,
  * przydatne: ```from collections import Counter``` - [docs](https://docs.python.org/3.6/library/collections.html#collections.Counter),

### 9. Naiwny klasyfikator bayesowski
  * wskazane odświeżenie twierdzenia Bayesa, w tym skąd nazwa "naiwny",
  * funkcja ```findall``` znajduje się w module ```re```, czyli musimy użyć ```import re```,
  * przydatne: ```from collections import defauldict``` - [docs](https://docs.python.org/3.6/library/collections.html#collections.defaultdict),
  * technika 'wygładzania' związana jest z pseudosumą,
  * zaimplementuj filtr antyspamowy wg opisu,
  * skorzystaj z datasetu ze [tutaj](https://spamassassin.apache.org/old/publiccorpus/),
  * osoby z nieparzystym numerem indeksu (albumu) opracowują zbiór z 2003 r., pozostałe z 2002 roku, 
  * proszę podzielić dane na treningowe i testowe z **innym** współczynnikiem niż 0.75,
  * prosze dokonać analizy wg schematu podanego w opisie,
  * na wyższą ocenę - za pomocą pakietu [scikit-learn](https://scikit-learn.org/stable/modules/naive_bayes.html) zaimplementuj wybraną wersję algorytmu 'Naive Bayes'.

### 10. Drzewa decyzyjne
  * przykład budowy drzewa decyzyjnego z obliczeniami - [tutaj](http://zsi.tech.us.edu.pl/~nowak/abdiped/dd.pdf),
  * zaimplementuj przykład budowy drzewa ID3 wg opisu, ale wygeneruj dodatkowe dane:
      * zbiór 20 kandydatów,
      * zbiór 50 kandydatów,
  * na wyższą ocenę - za pomocą pakietu [scikit-learn](https://scikit-learn.org/stable/modules/tree.html) wykorzystaj drzewa decyzyjne w dwóch wybranych problemach.

Zasady zaliczenia przedmiotu
======

1. Dowolny język do realizacji zadania. Aczkolwiek zalecany Python i jego biblioteki, takie jak Numpy, Scipy, Matplotlib, pandas itp.
  * idealny byłby Jupyter Notebook - <a href="https://www.dataquest.io/blog/jupyter-notebook-tutorial/">szybkie intro</a>
2. Zadanie należy zrealizować do następnych zajęć - max. 5 punktów,
  * Opóźnienie skutkuje wykonaniem części rozszerzonej danego zadania, która będzie określona przez prowadzącego,
  * Część rozszerzona będzie trudniejsza niż podstawowa i będzie konieczna rozmowa z prowadzącym o jej wykonaniu.
3. Zrealizowane zadanie należy umieścić na portalu GitHub (lub BitBucket) i wysłać link do repozytorium do prowadzącego (Slack).
4. Należy wykonać zrzuty ekranu z wynikami i również umieścić je w repozytorium.
5. Zadanie należy zaprezentować osobiście prowadzącemu (najlepiej na bieżących lub kolejnych zajęciach). 

Punkty i oceny
======

|    Liczba punktów    	| Ocena    |
|    :-------------:	| :-----:  |
|    45 - 50	        |     5    |
|    39 - 44	        |    4,5   |
|    34 - 38	        |     4    |
|    29 - 33	        |    3,5   |
|    25 - 28	        |     3    |
|     0 - 24	        |     2    |
