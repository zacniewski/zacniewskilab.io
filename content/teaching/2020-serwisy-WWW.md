---
title: "Projektowanie serwisów WWW"
subtitle: semestr zimowy 2020/2021
date: 2020-10-10
location: "Gdynia, Polska"
tags: ["courses", "www"]
---



Tematy zadań
======

### 1. Praca z systemem kontroli wersji. Podstawy WWW.
  * cel - stworzyć prostą stronę WWW, nie wymagającą użycia serwera webowego,
  * strona powinna zawierać linki do trzech podstron, które należy utworzyć:
      * strona z listem (odpowiednio sformatowany tekst, przypominający list),
      * strona z podręcznika, np. do informatyki (rysunki, tabele, wzory itp.),
      * strona z formularzem (inputy, pola tekstowe, checkboxy itp.), na początku bez walidacji wartości pól.
      * na stronie startowej należy umieścić informację o autorze oraz opcjonalnie np. logo, informacje o użytych technologiach itp.
  * należy użyć HTML, CSS i JS do ww. zadania,
  * na lab. nr 1 będziemy korzystać z [kursu o HTML i CSS](https://learn.shayhowe.com/html-css/),
  * co do JS, to wykorzystamy materiały z [kursu JS z 2020 roku](https://kursjs.pl/),
  * można wykorzystać darmowy szablon, np. [Bare, bazujący na Bootstrapie](https://startbootstrap.com/templates/bare/),
  * należy zapoznać się z modelem DOM: [tutaj](https://kursjs.pl/kurs/dom/dom.php),
  * nowe elementy HTML w miarę możliwości należy tworzyć i modyfikować za pomocą JS, przykłady [tutaj](https://www.w3schools.com/js/js_intro.asp),
  * należy utworzyć plik [README.md](https://www.markdownguide.org/basic-syntax/), który będzie zawierał opis repozytorium,
  * zrzut każdej z czterech utworzonych stron należy umieścić w pliku README.md jak obrazek i krótko opisać; jak to zrobić opisane jest w nw. linku o Markdown w sekcji 'Images'.
  

#### Git
  * książka online o systemie Git: [tutaj](https://git-scm.com/book/pl/v2), 
  * pomoc dla osób, które zaczynają przygodę z systemem kontroli wersji Git: [tutaj](https://www.flynerd.pl/2018/02/github-dla-zielonych-pierwsze-repozytorium.html),
  * należy utworzyć konto na hostingu GitHub lub innym (np. Bitbucket, GitLab), aby "przechowywać" tam zrealizowane laboratoria,
  * należy utworzyć repozytorium o nazwie `projektowanie-serwisow-www-dane-185ic`, gdzie zamaist 'dane' wpisujemy nazwisko lub nr indeksu; będzie ono podstawą zaliczenia przedmiotu.
 
### 2. Praca z elementami DOM + ćwiczenia
  * zadanie polega na wykorzystaniu Java Script (vanilla, jQuery lub inny) do wyświeltlania, modyfikowania, tworzenia (itp. itd.) elementów strony związanych z HTML i CSS,
  * należy użyć wybrany (dowolny) framework front-endowy do tego zadania,
  * jeden z najpopularniejszych to Bootstrap - [tutaj](https://getbootstrap.com/docs/4.5/getting-started/introduction/) (jest tutaj dobry, prosty startowy szablon),
  * UI Kit też jest ciekawy - [tutaj](https://getuikit.com/docs/installation),
  * przydatne strony o JS:
      * JS + HTML DOM na [W3 Schools](https://www.w3schools.com/jsref/default.asp).
  * na przykład korzystając z ww. szablonu Bootstrap, można dodać przycisk z klasą ```badge-light``` do strony (np. pod nagłówkiem 'Hello world'):
	```
	<button type="button" class="btn btn-primary">
			Notifications <span class="badge badge-light"></span>
		</button>
	```
	Poniżej, np. przed końcem sekcji ```body``` wrzucamy skrypt, który szuka elementu z klasą ```badge-light``` i ustawia jego wartość (innerHTML) na liczbę 6:
	```
	<script>
		  let x = document.getElementsByClassName("badge-light");
		  x[0].innerHTML = 6; // x[0] to pierwszy znaleziony element
	    </script>
	```
  * w podobny sposób należy wykorzystać inne dostępne metody i właściwości Java Script do modyfikacji elementów strony,
  * wskazane użycie 20 różnych metod i właściwości Java Script do ww. modyfikacji, np.:
  ```document.body.style.backgroundColor = "red";``` (właściwość 'backgroundColor') 
  lub  
  ```
  var node = document.createElement("LI");                 // Create a <li> node
  var textnode = document.createTextNode("Water");         // Create a text node
  node.appendChild(textnode);                              // Append the text to <li>
  document.getElementById("myList").appendChild(node);     // Append <li> to <ul> with id="myList"
```
  * w powyższym przykładzie użyte zostały trzy metody, ale całość traktujemy jako jeden przypadek z 20 wymaganych,
  * użyj w wybranych przez siebie fragmentach kodu "trybu ścisłego" - ```use strict;```
  * można do pomocy wykorzystać jQuery: [tutaj](https://www.w3schools.com/jquery/jquery_dom_get.asp)

### 3. Obsługa zdarzeń
* w ramach praktyki do wykonania [zadania z JavaScript - część 1](https://github.com/kartofelek007/zadania-podstawy/tree/master/3-funkcje/1-zadania) (9 zadań)
* za pomocą metody [addEventListener](https://www.w3schools.com/js/js_htmldom_eventlistener.asp) należy obsłużyć 10 różnych wybranych zdarzeń,
* lista zdarzeń HTML DOM do użycia [tutaj](https://www.w3schools.com/jsref/dom_obj_event.asp),
* przykład Keyboard Event na stronie [MDN](https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent),
* kody dla [klawiszy](http://keycode.info/)
* trochę przykładów: [plik JS z teorią i kodem](https://github.com/zacniewski/materials-about-internet-apps-and-www-websites/blob/main/zastosowania-java-script/scripts/3-event-handling.js) oraz odpowiadający mu [plik HTML](https://github.com/zacniewski/materials-about-internet-apps-and-www-websites/blob/main/zastosowania-java-script/event-handling.html),
* plusy za zróżnicowanie elementów za pomocą których realizowana będzie obsługa zdarzeń (elementy HTML, obiekt 'document', obiekt 'window') oraz za użycie metody removeEventListener().

### 4. Gra przeglądarkowa
* do zrealizowania jest gra przeglądarkowa, inna niż była omawiana na zajęciach,
* należy wykorzystać element <canvas> do implementowania podstawowych mechanik gry jak renderowanie, przesuwanie grafik, wykrywanie kolizji, mechanizmu sterowania oraz stanu wygrania/przegrania,
* nie należy korzystać z silników gier, dopuszczalna prosta biblioteka JS, np. jQuery
* wskazany dokładny opis tego, co robi dana funkcja lub wybrany fragment kodu,
* wskazane użycie event listenerów, obsługa klawiszy i/lub myszki, itp.,
* przykładowe gry dostępne [tutaj](https://dev.to/gscode/10-best-of-javascript-games-3mjj),
* plusy za modyfikacje, dobre komentarze i oryginalność (za wrzucenia gry bez zmian ocena '3').

### 5. Asynchroniczny Java Script
* ciekawie wyłumaczone zapytania asynchroniczne w JS: [tutaj](https://james-priest.github.io/udacity-nanodegree-mws/course-notes/asynchronous-javascript-requests.html),
* przykłady z zajęć dostępne na repo [tutaj](https://github.com/zacniewski/materials-about-internet-apps-and-www-websites/blob/main/zastosowania-java-script/scripts/5a-async.js) i [tutaj](https://github.com/zacniewski/materials-about-internet-apps-and-www-websites/blob/main/zastosowania-java-script/scripts/5-async.js),
* zasoby do użycia dostępne [tutaj](https://jsonplaceholder.typicode.com/) w dziale 'Resources' (6 rodzajów) lub [tutaj](https://restcountries.eu/), albo [tutaj](https://www.programmableweb.com/apis/directory)
* np. gdy chcemy uzyskać dostęp do usera nr 3 wpisujemy: https://jsonplaceholder.typicode.com/users/3
* gdy chcemy pobrać obiekt JSON z ww. strony, to można to zrobić np. za pomocą funkcji [getJSON](https://api.jquery.com/jquery.getjson/):  
```
$.getJSON( "https://jsonplaceholder.typicode.com/users/10", function( data ) {
  let name = data.name;
  let id = data.id;
  console.log( "User " + name + " has id=" + id); // User Clementina DuBuque has id=10
 });
```
* należy zrealizować po dwa przykłady do każdego z sześciu poniższych punktów, wykorzystując ww. API w punktach 2-6:
1. funkcja zwrotna (callback)
    - przykłady tworzenia funkcji zwrotnych: [tutaj](https://kursjs.pl/kurs/super-podstawy/funkcje-tematy-dodatkowe.php#callback) i [tutaj](https://javascript.info/callbacks),
    - utwórz obiekt JSON, zawierający w środku co najmniej podwójnie zagnieżdżone obiekty JSON;
    - ww. obiekty powinny zawierać w tablicach i wartościach obiektów liczby i łańcuch znakowe,
    - task1: wykorzystaj funkcję zwrotną do pobrania dwóch różnych wartości liczbowych (z różnych poziomów zagnieżdżenia) z ww. obiektu JSON i wykonaj wybrane działanie na tych liczbach,
    - task 2: wykorzystaj funkcję zwrotną do pobrania dwóch różnych łańcuchów znakowych (z różnych poziomów zagnieżdżenia) z ww. obiektu JSON i za pomocą [template strings](https://kursjs.pl/kurs/es6/template-strings.php) stwórz łańcuch znakowy z użyciem obu wcześniej wyekstrahowanych łańcuchów znakowych.  

2. obiekt Promise (resolve, reject) z metodami then(), catch() i finally() + axios (lub fetch)
    - task 1: wykorzystaj obiekt Promise do pobrania dwóch różnych zasobów liczbowych i napisz funkcję wykonującą wybrane działanie na tych liczbach,
    - task 2: wykorzystaj obiekt Promise do pobrania dwóch różnych dowolnych zasobów i napisz funkcję tworzącą z nich nowy obiekt,
    - należy pamiętać o obsłudze wszystkich pięciu metod obiektu Promise,
    - do pobierania zasobów należy wykorzystać metodę ```fetch``` lub bibliotekę ```axios```.

3. async/await + fetch (lub axios)
    - task 1: jak wyżej, 
    - task 2: jak wyżej,
    - należy stworzyć funkcje wykorzystujące składnię async/await,
    - do pobierania zasobów należy wykorzystać metodę ```fetch``` lub bibliotekę ```axios```.


4. Zapytania [AJAX](https://javascript.info/xmlhttprequest)
    - task 1: jak w pkt. 2 i 3,
    - task 2: jak w pkt. 2 i 3.
    - należy obsłużyć sukces zapytania (właściwość 'onload' obiektu XHR),
    - należy obsłużyć błąd zapytania (właściwość 'onerror' obiektu XHR).

    
5. metoda [fetch](https://kursjs.pl/kurs/ajax/fetch.php)
    - task 1: jak w pkt. 2 i 3,
    - task 2: jak w pkt. 2 i ,
    - nie używamy async/await.
    
6. bibliotexa [axios](https://github.com/axios/axios)
    - task 1: jak w pkt. 2 i 3,
    - task 2: jak w pkt. 2 i 3,
    - obiekt axios dostępny będzie jako ```response.data``` (patrz przykłady),
    - nie używamy async/await.
    - wystarczy użyć plików z CDN, opisanych w linku do biblioteki i umieścić je przed końcem sekcji 'body'.


### 6. React - aplikacja nr 1
* w aplikacji należy zrealizować nw. zadania,
  * tworzenie aplikacji za pomocą modułu 'create-react-app',
  * tworzenie komponentów funkcyjnych i klasowych,
  * należy utworzyć komponent nadrzędny i dwa komponenty potomne, mogą to być np. tabele, listy 'ol' lub 'ul', obrazki itp. Dowolność wyboru, aby nie były to przykłady zbyt zbliżone do tych z repo,
  * należy wykorzystać props (atrybuty, właściwosci) przy przekazywaniu danych do komponentu potomnego,
  * należy wykorzystać state (stan) przy tworzeniu komponentu klasowego,
  * należy dodać plik .css do wybranych komponentów,
  * należy zastosować funkcję ```map()``` przy generowaniu komponentów i należy pamiętać o atrybucie ```key```,
  * należy zrealizowac wysyłanie funkcji za pomocą 'props',
  * plusy za wykorzystanie [Bootstrapa](https://create-react-app.dev/docs/adding-bootstrap/) lub [react-bootstrap'a](https://react-bootstrap.netlify.app/getting-started/introduction) w create-react-app.


### 7. React - aplikacja nr 2 
* wykorzystanie [Material UI](https://material-ui.com/) oraz [React-Router](https://reactrouter.com/),
* [instalacja](https://material-ui.com/getting-started/installation/) Material UI,
* początki z [Material UI](https://material-ui.com/getting-started/learn/),
* przykład użycia Material UI z [freecodecamp](https://www.freecodecamp.org/news/meet-your-material-ui-your-new-favorite-user-interface-library-6349a1c88a8c/),
* routing po stronie serwera i po stronie klienta, czyli po co nam [Raect-Router](https://bulldogjob.pl/news/963-routing-po-stronie-klienta-z-react-router), proszę przeczytać przy ☕ lub 🍺 (małym),
* instalacja i proste przykłady użycia [React-Router'a](https://reactrouter.com/web/guides/quick-start),
* zadanie - wykorzystać dwie ww. biblioteki react'owe do utworzenia prostej SPA:
  * należy stworzyć trzy "podstrony" (komponenty ```link```, ```Switch``` i ```Route```),
  * na każdej z ww. podstron powinny znaleźć się trzy różne komponenty z Material UI,
  * plusy ➕➕➕ za wykorzystanie 'state', 'props', innych bibliotek (np. bootstrap).

### 8. React - aplikacja nr 2 + analiza kodu źródłowego + git difftool
* strona [nr 1](https://pl.reactjs.org/community/examples.html) z przykładami do analizy,
* strona [nr 2](https://reactjsexample.com/) z przykładami do analizy,
* przykłady wykorzystania komendy [git difftool](https://www.youtube.com/watch?v=Wk-IK2uJt28) (do obejrzenia przy ☕ lub 🍺)
* można wykorzystać inną wybraną przez siebie stronę z przykładami,
* należy rozsądnie wybrać przykład z jednej z ww. stron,
  * niektóre przykłady są dośc trywialne - ograniczają sie do instalacji wybranego pakietu i kilku prostych przypadków użycia,
* przykład należy przeanalizować i dokonać swoich modyfikacji,
* należy dokładnie opisać (skomentować) kod aplikacji (zainstalowane moduły, użyte komponenty itp.) oraz wprowadzone własne modyfikacje,
* ww. modyfikacje kodu należy zaprezentować wizualnie na zrzucie ekranu po komendzie ```git difftool```
* w pliku README należy umieścić link do oryginalnego przykładu,
* plusy ➕➕➕ za wykorzystanie 'spread operator', 'Object.assign', 'uniqid' i innych pakietów Reactów, elementów JS nie wykorzystywanych we wcześniejszych zadaniach.


Zasady zaliczenia przedmiotu
======

1. Dowolny język do realizacji zadań, tam gdzie da sie zastosować inne niż wskazane w danym laboratorium.
2. Zadanie należy zrealizować do kolejnych zajęć - max. 5 punktów:  
    * do tygodnia opóźnienia w realizacji zadania - max. 3 punkty,
    * przekroczenie obu terminów - max. 1 punkt,
    * ww. daty wskazane są w pliku z ocenami.
3. Zrealizowane zadanie należy umieścić na portalu GitHub (lub BitBucket) i wysłać link do prowadzącego (Slack).
4. Zadanie należy zaprezentować osobiście prowadzącemu na laboratorium (zdalnie popre udostępnianie ekranu). 
5. Pierwsze laboratorium jest "rozruchowe" - można oddawać zadania, ale nie trzeba.
6. Brak odpowiedniej liczby punktów spowoduje konieczność stworzenia nowych projektów.
7. Do każdego zadania dobrze jest dodać pliki (dodać do repozytorium) zawierające zrzuty działającej aplikacji.
Przyspieszy to proces sprawdzania. 
8. Brak znajomości utworzonej aplikacji powoduje obniżenie oceny.