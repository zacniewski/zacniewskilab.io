---
title: "Aplikacje internetowe"
subtitle: semestr zimowy 2020
date: 2020-10-01
location: "Gdynia, Polska"
tags: ["courses", "WWW"]
---

Tematy zadań
======
### 1. Blog uruchomiony na PaaS 
  * należy utworzyć repozytorium o nazwie `aplikacje-internetowe-dane-185ic`, 
    gdzie zamiast 'dane' wpisujemy nazwisko lub nr indeksu; będzie ono podstawą zaliczenia przedmiotu,
  * w ww. repozytorium umieszczamy kolejne zrealizowane zadania w osobnych folderach i z osobnymi plikami README.md,
  * [strona źródłowa](https://tutorial.djangogirls.org/pl/) do realizacji tego zadania,
  * poniższe zagadnienia powinny zostać zrealizowane:
      * instalacja pakietów i tworzenie projektu,
      * korzystanie z serwera deweloperskiego,
      * modele, migracje i ORM,
      * ustawienia projektu,
      * tworzenie superusera i panel admina,
      * tworzenie aplikacji w Django,
      * QuerySets i menadżery obiektów,
      * praca z plikami views.py, urls.py i szablonami,
      * praca z formularzmi (dodawanie posta, edycja istniejącego posta),
  * blog umieszczamy na platformie typu PaaS (np. [Heroku](https://www.heroku.com/home) lub inna, np. [Python Anywhere](https://www.pythonanywhere.com/)),
  * plusy za opracowanie widoku do [usuwania](https://docs.djangoproject.com/en/3.1/topics/db/queries/) postów,
  * dla Heroku konieczna będzie instalacja [Heroku-CLI](https://devcenter.heroku.com/articles/heroku-cli).
  

### 2. Rejestracja użytkowników
  * należy wykorzystać wbudowane [widoki uwierzytelniające](https://docs.djangoproject.com/en/3.1/topics/auth/default/#module-django.contrib.auth.views)
  * należy zrealizować następujące operacje:  
      * widoki 'login' i 'logout'
      * widok od zmiany hasła (PasswordChangeView)
      * widok od resetu hasła (PasswordResetView)
      * do ww. widoku należy użyć ConsoleBackend do wysyłania maili i porobić zrzuty ekranu
  * plus za widok realizujący rejestrację usera (signup),
  * plus za wrzucenia apki na wybrany PaaS.

### 3. Różne sposoby uwierzytelniania 
  * uwierzytelnianie przez social media i za pomocą wbudowanych backendów (username lub email),
  * należy dodać dwa dowolne backendy uwierzytelniające do listy AUTHORIZATION_BACKENDS (np. Facebook i Twitter),
  * konieczne będzie utworzenie aplikacji ww. portalach,
  * plusy za własne przemyślenia, analizę dokumentacji i idące za nimi modyfikacje w aplikacji.
  * plus za próbę wrzucenia apki na wybrany PaaS.


### 4. REST API z DRF
  * Django Rest Framework,
  * zezwolenia,
  * [Swagger](https://drf-yasg.readthedocs.io/en/stable/readme.html),
  * serializery,
  * należy przeanalizować i wdrożyć kod z repozytorium do zajęć, 
  * plusy za własne przemyślenia, analizę dokumentacji i idące za nimi modyfikacje w projekcie,
  * proponowane modyfikacje to np. nowa aplikacja, nowe API (v2), opcja wyszukiwania, 
    sprawdzenie innych serializerów, widoków, viewsets'ów, router'ów itp.

### 5. Web Scraping
  * krótki wstęp do web scrapingu z [Pythonem](https://realpython.com/python-web-scraping-practical-introduction/),
  * krótki wstęp do web scrapingu z [BS4](https://realpython.com/beautiful-soup-web-scraper-python/),
  * [Beautiful Soup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/),
  * [XPath](https://www.w3schools.com/xml/xpath_examples.asp) i [lxml](https://lxml.de/),
  * należy przeanalizować i wdrożyć kod z repozytorium do zajęć, na innych stronach niż te z przykładów,
  * konieczna jest analiza struktury strony, żeby wybrać odpowiednie elementy (w tym znaczniki HTML) do scrapowania,
  * plusy ➕ za implementację wyszukiwania elementów do scrapowania za pomocą Django, np.:  
    * należy utworzyć prosty formularz z jednym polem do wpisania elementu do wyszukania (scrapowania),
    * należy kod z zajęć umieścić w pliku views.py, w wybranym widoku,
    * wyniki scrapowania należy przekazać w funkcji render() do wybranego szablonu HTML i je tam wyświetlić,
  * plusy ➕ za własne przemyślenia, analizę dokumentacji i idące za nimi modyfikacje w projekcie.
  
### 6. Zezwolenia i uwierzytelnianie w DRF
  * to kontynuacja lab. nr 4,
  * viewsets,
  * routers,
  * uwierzytelnianie (basic, session, token),
  * do poczytania 📕 📖 świetny [artykuł](https://testdriven.io/blog/django-spa-auth/) o autoryzacji dla zestawu Django + DRF + React,
  * do poczytania o zastosowaniu [JWT](https://jwt.io/introduction),
  * należy przeanalizować i wdrożyć kod z repozytorium do zajęć, 
  * plusy za własne przemyślenia, analizę dokumentacji i idące za nimi modyfikacje w projekcie,
  * plus za implementację [prostego licznika wizyt z użyciem cookies](https://www.tangowithdjango.com/book17/chapters/cookie.html) (p. 11.5 w linku, ale wstęp też jest przyjemny do analizy),
  * cookie można ustawić w dowolnym widoku, nie trzeba (ale można) tworzyć nowego widoku w tym celu, [tutaj](https://medium.com/better-programming/managing-cookies-in-django-34981d9bf0ae) drugi link do przykładu z cookie,
  * proponowane modyfikacje to np. nowa aplikacja, nowe API (v2), opcja wyszukiwania, 
    sprawdzenie innych serializerów, widoków, viewsets'ów, router'ów itp.

### 7. Python + Redis + Django
  * oficjalna [strona Redisa](https://redis.io/),
  * seria artykułów o współpracy [Python-Redis](https://mmazurek.dev/tag/redis-i-python/?order=asc),
      - należy przerobić zadania zawarte w powyższych artykułach i dodać je do swojego repo,
      - nie jest konieczna instalacja Redisa za pomocą Dockera, wystarczy standardowa instalacja, jak pokazano w poniższych linkach,
      - oprócz instalacji ```redis-server``` konieczna jest instalacja pakietu ```redis``` poprzez 'pip', co również jest pokazane w ww. artykułach,
      - najwygodniej jest podzielić się zadaniami, żeby przyspieszyć pracę z Redisem,
  * sposób instalacji dla [systemów Windows](https://medium.com/cook-php/how-to-run-redis-on-windows-97b829a42486),
  * [repo](https://github.com/tporadowski/redis/tree/win-5.0) z ww. artykułu oraz [instalki](https://github.com/tporadowski/redis/releases),
  * tutorial o współpracy [Python + Redis](https://realpython.com/python-redis/),
  * do przerobienia przykład współpracy [Django + Redis + Celery](https://stackabuse.com/asynchronous-tasks-in-django-with-redis-and-celery/),
  * praca z [workerami w Celery](https://docs.celeryproject.org/en/stable/userguide/workers.html) w Celery,
  * praca z [Celery Beat](https://www.merixstudio.com/blog/django-celery-beat/),
  * należy przeanalizować, zmodyfikować i wdrożyć kod z ww. strony w wybranym prze siebie celu, 
  * plusy za własne przemyślenia, analizę dokumentacji i idące za nimi modyfikacje w aplikacji (standardowe taski oraz taski okresowe w Celery, dodawanie tasku za pomocą wybranego zdarzenia, itp.).


### 8. Czat z użyciem Web Socket + Web Workers
  * part I:
    * szybkie wyjaśnienie idei [WebSockets](https://bulldogjob.pl/news/751-prosto-o-websocket),
    * więcej szczegółów na [javascript.info](https://javascript.info/websocket),
    * moduł[ws](https://github.com/websockets/ws) z Node.js,
    * czym jest [WSGI](https://www.fullstackpython.com/wsgi-servers.html)?,
    * od Django 3.0 możemy również korzystać z zalet [ASGI](https://arunrocks.com/a-guide-to-asgi-in-django-30-and-its-performance/) (Asynchronous Server Gateway Interface),
    * gdzie można korzystać z [ASGI](https://github.com/florimondmanca/awesome-asgi),
    * przykład [chata](https://github.com/narrowfail/django-channels-chat) z wykorzystaniem django-channels,
    * przykład z wykorzystaniem [django-channels](https://medium.com/@ksarthak4ever/django-websockets-and-channels-85b7d5e59dda) do analizy i realizacji,
    * przykład z wykorzystaniem [socket.io](https://socket.io/get-started/chat/) do budowy czata,
    * należy przejrzeć materiały z powyższych stron oraz wdrożyć przykład z wybranego linku z django-channels lub socket.io, przy okazji komentując najważniejsze elementy kodu.

  * part II:
    * wyjaśnienie idei stosowania [Web Workerów](https://www.w3schools.com/html/html5_webworkers.asp),
    * praca z [Web Worker'ami](https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API/Using_web_workers),
    * należy zwrócić uwagę na typowe dla Workerów elementy: event listener ```onmessage``` i funkcję ```postMessae()```,
    * należy wdrożyć dwa Web Workery, z których każdy wykonuje "czasochłonne" obliczenia, np. obliczanie liczby ciągu Fibonnaciego.
  * plusy za własne przemyślenia, analizę dokumentacji i idące za nimi modyfikacje w kodzie.

### 9. Django + React (aplikacja CRUD)
  * backend napisany w Django,
  * frontend napisany za pomocą React.js,
  * biblioteka 'axios' użyta do "konsumowania" API wystawionego np. przez DRF,
  * przykład aplikacji [CRUD](https://bezkoder.com/django-react-axios-rest-framework/) z wykorzystaniem Django i React'a,
  * należy przeanalizować i wdrożyć kod z ww. poradnika, 
  * plusy za własne przemyślenia, analizę dokumentacji i idące za nimi modyfikacje w aplikacji.


### 10. Django + React (aplikacja typu ToDo)
  * backend napisany w Django,
  * frontend napisany za pomocą React.js,
  * biblioteka 'axios' użyta do "konsumowania" API wystawionego np. przez DRF,
  * przykład aplikacji [To-Do](https://www.digitalocean.com/community/tutorials/build-a-to-do-application-using-django-and-react) z wykorzystaniem Django i React'a,
  * należy przeanalizować i wdrożyć kod z ww. poradnika, 
  * plusy za własne przemyślenia, analizę dokumentacji i idące za nimi modyfikacje w aplikacji.

Zasady zaliczenia przedmiotu
======

1. Dowolny język do realizacji zadania, ale preferowany zestaw to Python + Django. Inne opcje po konsultacji z prowadzącym.
2. Zadanie należy zrealizować do kolejnych zajęć - max. 5 punktów:  
    * do tygodnia opóźnienia w realizacji zadania - max. 3 punkty,
    * przekroczenie obu terminów - max. 1 punkt,
    * ww. daty wskazane są w pliku z ocenami.
3. Zrealizowane zadanie należy umieścić na portalu GitHub (lub innym) i wysłać link do prowadzącego (Slack).
4. Zadanie należy zaprezentować osobiście prowadzącemu na laboratorium (zdalnie popre udostępnianie ekranu). 
5. Pierwsze laboratorium jest "rozruchowe" - można oddawać zadania, ale nie trzeba.
6. Brak odpowiedniej liczby punktów spowoduje konieczność stworzenia nowych projektów.
7. Do każdego zadania dobrze jest dodać pliki (dodać do repozytorium) zawierające zrzuty działającej aplikacji.
Przyspieszy to proces sprawdzania. 
8. Brak znajomości utworzonej aplikacji powoduje obniżenie oceny.