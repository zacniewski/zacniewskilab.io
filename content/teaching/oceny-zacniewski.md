---
title: "Zaliczenia i oceny"
collection: teaching
type: "Ewaluacja"
permalink: /teaching/zaliczenia-i-oceny-2019
venue: "AMW, WEM"
date: 2019-10-08
location: "Gdynia, Polska"
---
**Bieżący semestr:**  

Oceny zaliczeniowe - Projektowanie serwisów WWW (185IC): [tutaj](https://docs.google.com/spreadsheets/d/1PhoihRYm-9aK4XkBSO9pQ1tCZ5M85QbIcl1_21C47n4/edit?usp=sharing)  
Oceny zaliczeniowe - Aplikacje internetowe (185IC): [tutaj](https://docs.google.com/spreadsheets/d/1M0Od0GsMsWJB-SKi8K2QvxBSEVlZggMvu6knLPmQxtA/edit?usp=sharing)  
Oceny zaliczeniowe - Wstęp do ML (175IC): [tutaj](https://docs.google.com/spreadsheets/d/1tqJbk-9-TQO_bjZBeRzTOlj6aB3gOkea5nOuO6Ne2L8/edit?usp=sharing)  
<!---Oceny zaliczeniowe - aplikacje mobilne (185IC): [tutaj](https://docs.google.com/spreadsheets/d/1nfwlRkYTF8COpy_aCfD1NEGkrzp7ARb7x0_W0nQvaPI/edit?usp=sharing).--->

**Poprzednie semestry:**  

Oceny zaliczeniowe - Android (175IC): [tutaj](https://docs.google.com/spreadsheets/d/1h2LvIGQ0I6I6j27n7Ez7625NdQiGr2TZGD_rx-t8ayo/edit?usp=sharing)  
Oceny zaliczeniowe - Projektowanie serwisów WWW (175IC): [tutaj](https://docs.google.com/spreadsheets/d/1GU80636RGj1TKG6j2YZHoZseTRmTTsoAbvdpkigyBcU/edit?usp=sharing)  
Oceny zaliczeniowe - Android (165IC): [tutaj](https://docs.google.com/spreadsheets/d/1q18Zv51akzAvTbD0kPMY44FSwlEJcpEPQFwgWs8K9FY/edit?usp=sharing)  
Oceny zaliczeniowe - Data Science (165IC): [tutaj](https://docs.google.com/spreadsheets/d/1D-BSg3D5i02HLtd3cW69dfdCfTMp0_CT_XyOCDbVz2s/edit?usp=sharing)  


