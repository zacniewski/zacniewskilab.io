---
title: "React.js"
date: 2020-09-01
location: "Gdynia, Polska"
tags: ["courses", "WWW"]
---

### 1. Kursy
  * oficjalna dokumentacja: [tutaj](https://pl.reactjs.org/)
  * oficjalny tutorial: [tutaj](https://reactjs.org/tutorial/tutorial.html)
  * ... i po polsku: [tutaj](https://pl.reactjs.org/tutorial/tutorial.html)
  * React od podstaw na TypeOfWeb: [tutaj](https://typeofweb.com/wprowadzenie-kurs-react-js/)
  * tutorial na W3 Schools: [tutaj](https://www.w3schools.com/react/default.asp)

### 2. Porady
  * kiedy używać klamrowych nawiasów przy imporcie: [tutaj](https://stackoverflow.com/questions/36795819/when-should-i-use-curly-braces-for-es6-import/36796281#36796281)
  * atrybuty HTML w React: [tutaj](https://pl.reactjs.org/docs/dom-elements.html)
  
### 3. Ściągi
  * ściąga z Reacta 15 i 16 [pobierz](https://devhints.io/react) 

### 4. Tools
  * Gatsby - framework do tworzenia stron i apek, oparty na React'ie: [tutaj](https://www.gatsbyjs.com/)
  * Material UI - komponenty Reacta: [tutaj](https://material-ui.com/)
  * React Query - data synchronization for React: [tutaj](https://www.youtube.com/watch?v=TUa2l2099sQ)
  