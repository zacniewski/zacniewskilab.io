---
title: "Protokół HTTP"
date: 2020-09-01
location: "Gdynia, Polska"
tags: ["courses", "WWW"]
---



### 1. Podstawowe pojęcia
  * Protokół Przesyłania Danych Hipertekstowych (Hypertext Transfer Protocol, HTTP) to protokół warstwy aplikacji, odpowiedzialny za transmisję dokumentów hipermedialnych, jak np. HTML. Został stworzony do komunikacji pomiędzy przeglądarkami, a serwerami webowymi, ale może być używany również w innych celach. HTTP opiera się na klasycznym modelu klient-serwer, gdzie klient inicjuje połączenie poprzez wysłanie żądania, następnie czeka na odpowiedź. HTTP jest protokołem bezstanowym, co oznacza, że serwer nie przechowuje żadnych danych (stanów) pomiędzy oboma żądaniami. Mimo, że często opiera się na warstwie TCP/IP, może być używany także na godnej zaufania warstwie transportowej, tj. protokół, który nie traci po cichu komunikatów, jak ma to miejsce w przypadku UDP. RUDP, wiarygodna aktualizacja UDP, może stanowić odpowiednią alternatywę.

  * początki pracy w sieci WWW: [tutaj](https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web)
  * jak działa sieć WWW [tutaj](https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web/How_the_Web_works)

### 2. Elementy protokołu HTTP
  * idea pobierania strony WWW: ![fetch web page image](https://media.prod.mdn.mozit.cloud/attachments/2016/08/09/13677/d031b77dee83f372ffa4e0389d68108b/Fetching_a_page.png)
  * założenia protokołu HTTP: [tutaj](https://developer.mozilla.org/en-US/docs/Web/HTTP/Overview)
  * metody protokołu HTTP: [tutaj](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods)
  * cache: [tutaj](https://developer.mozilla.org/en-US/docs/Web/HTTP/Caching)
  * ciasteczka (ang. cookies): [tutaj](https://developer.mozilla.org/en-US/docs/Web/HTTP/Cookies)


### 3. Pliki do pobrania

  * Podstawy protokołu HTTP by Sekurak [pobierz](https://cdn.sekurak.pl/Podstawy_HTTP_v10.pdf) 
  