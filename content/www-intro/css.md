---
title: "CSS"
date: 2020-08-01
location: "Gdynia, Polska"
tags: ["courses", "WWW"]
---

### Linki 
  * kurs CSS Mirosława Zelenta: [tutaj](https://miroslawzelent.pl/kurs-css/)
  * podstawy CSS na Khan Academy: [tutaj](https://pl.khanacademy.org/computing/computer-programming/html-css/intro-to-css/pt/css-basics)
  * podstawy CSS na stronie Mozilli: [tutaj](https://developer.mozilla.org/en-US/docs/Learn/CSS)
  * kanał na You Tube ["Hello Roman"](https://www.youtube.com/channel/UCq8XmOMtrUCb8FcFHQEd8_g) o frontendzie
  * [Block Element Modifier](http://getbem.com/)
  
### Bootstrap
  * jak zacząć pracę z Bootstrapem: [tutaj](https://www.toptal.com/front-end/what-is-bootstrap-a-short-tutorial-on-the-what-why-and-how)
  * dobre praktyki w Bootstrapie: [tutaj](https://www.toptal.com/twitter-bootstrap/speeding-up-development-bootstrap)
  * najczęstsze błędy w Bootstrapie: [tutaj](https://www.toptal.com/twitter-bootstrap/the-10-most-common-bootstrap-mistakes)
  * Bootstrap Studio: [tutaj](https://bootstrapstudio.io/)