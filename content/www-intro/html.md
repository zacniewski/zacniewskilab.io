---
title: "HTML i WWW"
date: 2020-08-01
location: "Gdynia, Polska"
tags: ["courses", "WWW"]
---

### 1. Kursy
  * jak zacząć pracę z HTML i CSS: [tutaj](https://learn.shayhowe.com/html-css/)
  * kurs HTML Mirosława Zelenta: [tutaj](https://miroslawzelent.pl/kurs-html/)
  * podstawy HTML na Khan Academy: [tutaj](https://pl.khanacademy.org/computing/computer-programming/html-css/intro-to-html/pt/html-basics)
  * podstawy HTML na stronie Mozilli: [tutaj](https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML)

### 2. Ciekawostki
  * trochę krytyki dotczącej tworzenia stron WWW: [tutaj](https://www.webkrytyk.pl/)
  * ciekawostki ze świata WWW (po polsku): [tutaj](https://blog.comandeer.pl/)
  * BrowSH - fully-modern text-based browser: [tutaj](https://www.brow.sh/)