---
title: "Java Script soon"
date: 2020-08-01
location: "Gdynia, Polska"
tags: ["courses", "WWW"]
---

### 1. Kursy
  * podstawy Java Script na stronie Mozilli: [tutaj](https://developer.mozilla.org/en-US/docs/Learn/JavaScript)
  * i po polsku również na MDN: [tutaj](https://developer.mozilla.org/pl/docs/Web/JavaScript/Ponowne_wprowadzenie_do_JavaScript)
  * baza wiedzy o Java Script na stronie Mozilli: [tutaj](https://developer.mozilla.org/bm/docs/Web/JavaScript)
  * tutorial na W3 Schools: [tutaj](https://www.w3schools.com/js/default.asp)
  * interaktywny kurs online: [tutaj](https://learnjavascript.online/app.html)
  * kurs JavaScript z 2020 roku: [tutaj](https://kursjs.pl/)
  
### 2. Linki o JS
  * format JSON: [tutaj](https://www.json.org/json-pl.html)
  * wybrane nowości z ES6: [tutaj](https://www.w3schools.com/js/js_es6.asp)
  * JSFuck - tylko 6 znaków wystarczy do pisania w JavaScript: [tutaj](http://www.jsfuck.com/)
  * gra sprawdzająca znajomość JavaScript: [tutaj](https://eqeq.js.org/)

### 3. Biblioteki
  * jQuery: [tutaj](https://jquery.com/)
  * Isotope: [tutaj](https://isotope.metafizzy.co/)