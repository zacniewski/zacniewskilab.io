---
title: "System kontroli wersji GIT"
date: 2020-09-01
location: "Gdynia, Polska"
tags: ["courses", "WWW"]
---

### 1. Podstawowe pojęcia
  * System kontroli wersji śledzi wszystkie zmiany dokonywane na pliku (lub plikach) i umożliwia przywołanie dowolnej wcześniejszej wersji. Przykłady na zajęciach będą śledziły zmiany w kodzie źródłowym, niemniej w ten sam sposób można kontrolować praktycznie dowolny typ plików.
  * idea rozproszonego systemu kontroli wersji: ![vcs image](https://git-scm.com/book/en/v2/images/distributed.png)

  * Jeśli jesteś grafikiem lub projektantem WWW i chcesz zachować każdą wersję pliku graficznego lub układu witryny WWW (co jest wysoce prawdopodobne), to używanie systemu kontroli wersji (ang. VCS-Version Control System) jest bardzo rozsądnym rozwiązaniem. Pozwala on przywrócić plik(i) do wcześniejszej wersji, odtworzyć stan całego projektu, porównać wprowadzone zmiany, dowiedzieć się kto jako ostatnio zmodyfikował część projektu powodującą problemy, kto i kiedy wprowadził daną modyfikację. Oprócz tego używanie VCS oznacza, że nawet jeśli popełnisz błąd lub stracisz część danych, naprawa i odzyskanie ich powinno być łatwe. Co więcej, wszystko to można uzyskać całkiem niewielkim kosztem.

### Linki 
  * wprowadzenie do kontroli wersji: [tutaj](https://git-scm.com/book/pl/v2/Pierwsze-kroki-Wprowadzenie-do-kontroli-wersji)
  * podstawy Git: [tutaj](https://git-scm.com/book/pl/v2/Pierwsze-kroki-Podstawy-Git)
  * najpopularniejsze komendy Git: [tutaj](http://strefakodera.pl/programowanie/git/do-pobrania-sciaga-z-podstawowymi-komendami-gita)
  * wizualna ściąga do Gita: [tutaj](https://marklodato.github.io/visual-git-guide/index-pl.html)


### Pliki do pobrania

1. Ściąga z komendami Gita [pobierz](http://strefakodera.pl/wp-content/uploads/2016/07/git-sciaga.pdf) 
  