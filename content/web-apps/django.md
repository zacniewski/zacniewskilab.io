---
title: "Django"
date: 2020-09-01
location: "Gdynia, Polska"
tags: ["courses", "WWW"]
---

### 1. Wprowadzenie
* Django (czytaj: dżango) to darmowy i open-source'owy framework do tworzenia aplikacji webowych, napisany w Pythonie. To taki zbiór narzędzi, dzięki którym tworzenie stron internetowych jest szybsze i łatwiejsze. Gdy budujesz stronę internetową, często potrzebujesz tych samych rzeczy: uwierzytelnić użytkowników (rejestracja, logowanie, wylogowywanie), panel zarządzania stroną, formularzy, sposobu przesyłania plików itd.

* Na szczęście pewni programiści już kiedyś zauważyli, że wszyscy web developerzy stają przed takimi samymi problemami i stworzyli frameworki (Django jest jednym z nich), które dostarczają nam gotowych do użycia komponentów. Frameworki istnieją, by oszczędzić Ci wyważania otwartych drzwi i ułatwić Ci proces tworzenia nowej strony.

### 2. Linki 
  * oficjalna dokumentacja: [tutaj](https://docs.djangoproject.com/pl/3.1/intro/overview/)
  * najlepszy polski kurs Django: [tutaj](https://tutorial.djangogirls.org/pl/)
  * sporo wskazówek jak zacząć pracę z Django: [tutaj](https://learndjango.com/tutorials/how-learn-django)
  * tutorial Django na Mozilli: [tutaj](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django)
  * bardzo dobre angielskie tutoriale Django: [tutaj](https://realpython.com/tutorials/django/)
  * dużo dobrych porad dotyczących Django: [tutaj](https://simpleisbetterthancomplex.com/)
  * kolejne porady dotyczące Django: [tutaj](https://www.laac.dev/blog/)
  * tutorial Django na Data Flair: [tutaj](https://data-flair.training/blogs/django-orm-tutorial/)

### 3. Tips
  * [najczęstsze błędy w Django](https://www.toptal.com/django/django-top-10-mistakes)
  * Class Based Views w Django: [tutaj](https://ccbv.co.uk/)
  * CBV vs FBV w Django: [tutaj](https://simpleisbetterthancomplex.com/article/2017/03/21/class-based-views-vs-function-based-views.html)
  * pakiety Django: [tutaj](https://djangopackages.org/)
  * czy musimy łączyć Django np. z Reactem? [tutaj](https://vsupalov.com/do-i-need-to-combine-django-with-vue-or-react/)

### 4. Django + Celery
  * asynchroniczne zadania z Django i Celery: [RealPython](https://realpython.com/asynchronous-tasks-with-django-and-celery/)
  * asynchroniczne zadania z Django i Celery: [TestDriven](https://testdriven.io/blog/django-and-celery/)
  * planowanie "nudnych " zadań z Celery: [tutaj](https://www.merixstudio.com/blog/django-celery-beat/)
  * zadania asynchroniczne w Django z Redisem i Celery: [tutaj](https://stackabuse.com/asynchronous-tasks-in-django-with-redis-and-celery/)
  * kilka tricków w Celery:  [tutaj](https://djangostars.com/blog/the-python-celery-cookbook-small-tool-big-possibilities/)

### Django + DRF
  * strona domowa DRF: [tutaj](https://www.django-rest-framework.org/)
  * sporo praktycznych porad o połaczeniu Django z DRF: [tutaj](https://www.valentinog.com/blog/drf/)

  