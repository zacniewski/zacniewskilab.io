---
title: "Python"
date: 2020-08-01
location: "Gdynia, Polska"
tags: ["courses", "Python"]
---

### Linki 
  * lista zasobów związanych z Pythonem: [tutaj](https://github.com/zacniewski/useful-python-resources)
  * OOP w Pythonie: [tutaj](https://realpython.com/python3-object-oriented-programming/)
  