---
title: Useful tips and links
comments: false
---

### Praca i porady

  * [porady doświadczonego programisty](https://github.com/guettli/programming-guidelines)
  * [praca dla startujących w IT](https://justjoin.it/junior/)
  * [początki w IT bez ściemy](https://nofluffjobs.com/pl/juniors/)

### Free resources
  * [darmowa książka IT codziennie](https://www.packtpub.com/free-learning)
  * [książki polecane przez Toptal](https://www.toptal.com/software/toptal-s-list-of-top-free-programming-books)
  * [kolekcje darmowych zdjęć](http://thestocks.im/#)

### Przetwarzanie obrazów w przeglądarce
  * [wstęp do Computer Vision z użyciem OpenCV.js](https://www.digitalocean.com/community/tutorials/introduction-to-computer-vision-in-javascript-using-opencvjs)
  * [konfiguracja OpenCV.js](https://medium.com/analytics-vidhya/how-to-install-opencv-js-c718d2add936)
  * 

### Python
  * [uruchamianie komend shell'owych za pomocą Pythona](https://stackabuse.com/executing-shell-commands-with-python/)
  * [Arcade - framework do tworzenia gier w Pythonie](https://realpython.com/arcade-python-game-framework/)
  * [tworzenie gier w Pythonie za pomocą PyGame](https://realpython.com/pygame-a-primer/)
  * [jak stworzyć grę typu multiplayer w Pythonie i JavaScript](https://dev.to/aduranil/53-learnings-from-writing-a-multiplayer-strategy-game-3ijd)


### Java Script
  * [różnica między document, window a screen w Java Script](https://stackoverflow.com/questions/9895202/what-is-the-difference-between-window-screen-and-document-in-javascript)
  * [profesjonalne animacje w JS](https://greensock.com/gsap/)
  * [Key codes](https://keycode.info/)

### HTML, CSS, Markdown
  * [zalecenia do HTML i CSS od Google](https://google.github.io/styleguide/htmlcssguide.html)
  * [podstawowa składnia Markdown](https://www.markdownguide.org/basic-syntax/)
  * [Tailwind CSS](https://tailwindcss.com/)

### IDE i konsole
  * [PyCharm i inne od JetBrains](https://www.jetbrains.com/products.html#) - promocje dla studentów!
  * [Visual Studio Code](https://code.visualstudio.com/)
  * [Portable console emulator for Windows](https://cmder.net/)
