---
title: About me
# subtitle: Why you'd want to hang out with me
comments: false
---
I'm an educator and Scientific Developer from Poland, who focuses on Internet Technologies and Computer Vision, and Data Science. I'm involved in scientific and commercial projects, related to these areas.  

I prefer programming and scientific research the most, and I also conduct lectures and laboratories on "Computer Vision", "WWW technologies" and "Introduction to Data Science". I was a supervisor of many engineering projects. I also worked as a developer (Python, WWW, C++) in a few IT companies.  

By the way, you can check my IT and scientific profiles to look at my interests and to discover what I'm currently working on. You can contact me through links at the bottom.  

### Work, work, work

Recently, I had a big pleasure to be a Scientific Developer in [Toucan Systems](https://ai.toucan-systems.pl/) AI team. It is a tech company that connects the worlds of science, engineering and the arts :)
